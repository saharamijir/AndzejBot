from .baseModel import BaseModel
from peewee import *


class Playlist(BaseModel):
    songname = CharField(max_length=50)
    url = CharField()
    thumbnailurl = CharField()
