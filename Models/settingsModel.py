from peewee import *
from .baseModel import BaseModel


class Settings(BaseModel):
    name = CharField()
    value = CharField()
