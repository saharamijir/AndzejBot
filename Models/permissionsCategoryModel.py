from peewee import *
from Models.baseModel import BaseModel


class PermissionsCategory(BaseModel):
    name = CharField(max_length=50)
