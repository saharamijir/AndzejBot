from .roleModel import Role
from .permissionModel import Permission
from .baseModel import BaseModel
from peewee import *


class RolePermission(BaseModel):
    role = ForeignKeyField(Role, backref="permissions")
    permission = ForeignKeyField(Permission, backref="roles")
