from peewee import *
DATABASE = '../andzej.db'
database = SqliteDatabase(DATABASE)


class BaseModel(Model):
    id = AutoField()

    class Meta:
        database = database
