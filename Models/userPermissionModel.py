from peewee import *
from Models.baseModel import BaseModel
from Models.permissionModel import Permission
from Models.userModel import User


class UserPermision(BaseModel):
    id = AutoField()
    user = ForeignKeyField(User, backref='permissions')
    permission = ForeignKeyField(Permission, backref='users')
