from peewee import *
from Models.baseModel import BaseModel


class User(BaseModel):
    id = AutoField()
    name = CharField(max_length=50)
    chips = IntegerField()
