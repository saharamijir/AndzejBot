from peewee import *
from Models.baseModel import BaseModel
from Models.permissionsCategoryModel import PermissionsCategory


class Permission(BaseModel):
    name = CharField(max_length=50)
    category = ForeignKeyField(model=PermissionsCategory, backref='permissions')
