from peewee import *
from .baseModel import BaseModel


class Role(BaseModel):
    name = CharField()
