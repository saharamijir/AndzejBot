from Models.baseModel import BaseModel
from Models.userModel import User
from peewee import *


class Request(BaseModel):
    name = CharField()
    date = DateField()
    author = ForeignKeyField(User, backref="requests")
