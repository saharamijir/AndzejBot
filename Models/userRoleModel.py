from peewee import *
from .baseModel import BaseModel
from .userModel import User
from .roleModel import Role


class UserRole(BaseModel):
    user = ForeignKeyField(User, backref="roles")
    role = ForeignKeyField(Role, backref="users")