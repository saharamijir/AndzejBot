"""
Discord bot written in Python 3
"""
import discord
from discord.ext import commands
import logging
from utils.Shitposts import FourChan
import os
import json


def Main():
    with open('settings.json') as file:
        settings = json.load(file)
    command_prefix = "a!"
    client = commands.Bot(command_prefix)
    SetLogger()

    @client.event
    async def on_ready():
        print('Logged in as')
        print(client.user.name)
        print(client.user.id)
        print('------')
        LoadInitialExtensions(client)
        await client.change_presence(game=discord.Game(name=settings["description"]))


    @client.event
    async def on_message(message):
        if "wtf" in message.content.lower() or "nie zrobisz tego" in message.content.lower():
            imageToSend = FourChan().RandomShitpost()
            await client.send_file(message.channel, imageToSend)
            os.remove(imageToSend)
        await client.process_commands(message)

    client.run(settings["token"])


def LoadInitialExtensions(client):
    extensions = ('cogs.manage', 'cogs.music', 'cogs.blade_and_soul')
    logger = logging.getLogger('discord')
    for extension in extensions:
        try:
            client.load_extension(extension)
            logger.info('Loaded extension {}'.format(extension))
        except Exception as e:
            exc = '{}: {}'.format(type(e).__name__, e)
            logger.error('Failed to load extension {}\n{}'.format(extension, exc))


def SetLogger():
    loggerFormat = '%(levelname)s\t %(asctime)s\t %(name)s %(message)s;'

    logger = logging.getLogger('discord')
    logger.setLevel(logging.DEBUG)
    handler = logging.FileHandler(filename='discord.log',
                                  encoding='utf-8',
                                  mode='w')
    handler.setFormatter(logging.Formatter(loggerFormat))
    logger.addHandler(handler)
    return logger


if __name__ == "__main__":
    Main()


