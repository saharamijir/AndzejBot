import discord
from discord.ext import commands
from utils.BladeAndSoulApi import BladeAndSoulApi


class BladeAndSoul():
    def __init__(self, bot):
        self.bot = bot
        self.BnS = BladeAndSoulApi()

    @commands.group(pass_context=True)
    async def bns(self, ctx):
        if ctx.invoked_subcommand is None:
            await self.bot.say('Nie ma takiej komendy debilu')

    @bns.command()
    async def character(self, charName, region):
        character = self.BnS.GetCharacter(region, charName)
        embed = discord.Embed(title=character["characterName"], color=0x48e421)
        embed.set_image(url=character["characterImg"])
        embed.add_field(name="Level", value=str(character["playerLevel"]) + '('+str(character["playerLevelHM"])+')')
        embed.add_field(name="Current Element", value=character["activeElement"])
        embed.add_field(name="Attack Power", value=character["ap"])
        embed.add_field(name="Defense", value=character["defence"])
        embed.add_field(name="HP", value=character["hp"])

        await self.bot.say(embed=embed)

    @bns.command()
    async def item(self, itemName):
        itemInfo = self.BnS.GetItemInfoByName(itemName)
        embed = discord.Embed(title=itemName, color=0x48e421)
        embed.set_image(url=itemInfo["img"])
        embed.add_field(name="Type", value=itemInfo["type"])
        embed.add_field(name="Stats", value=itemInfo["stats"])
        embed.add_field(name="Level", value=itemInfo["characterLevel"])

        await self.bot.say(embed=embed)

def setup(bot):
    bot.add_cog(BladeAndSoul(bot))


