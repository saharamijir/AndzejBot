import discord
from discord.ext import commands
import logging
import datetime

class Misc():
    def __init__(self, bot):
        self.bot = bot
        self.logger = logging.getLogger("discord")

    @commands.command(pass_context=True)
    async def request(self, ctx, *, request: str):
        with open('requests.txt', 'a+') as file:
            file.write('{}\tRequest: {}\tby: {}\n'.format(ctx.message.timestamp.strftime("%Y-%m-%d %H:%M"),
                                                          request,
                                                          ctx.message.author))
        await self.bot.say("Thanks for the request")

    @commands.command(aliases=["requests", "showrequest", "allrequests"])
    async def showRequests(self):
        with open('requests.txt', 'r') as file:
            requests = file.read().splitlines()
        await self.bot.say('\n'.join(requests))


def setup(bot):
    bot.add_cog(Misc(bot))
