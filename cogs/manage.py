import discord
from discord.ext import commands
import logging
from utils.Checks import *


class Manage:
    def __init__(self, bot):
        self.bot = bot
        self.logger = logging.getLogger('discord')

    @commands.group(pass_context=True, hidden=True)
    @is_owner()
    async def manage(self, ctx):
        """
        Command group for managing bot, only bot author can use those commands
        :param ctx:
        :return:
        """
        if ctx.invoked_subcommand is None:
            await self.bot.say('Nie ma takiej komendy debilu')

    @manage.error
    async def manage_error(self, error, ctx):
        if isinstance(error, commands.CheckFailure):
            self.logger.info('Unauthorized attempt to use manage module')
            await self.bot.say("Nie masz uprawnień do kożystania z tego modułu")

    @manage.command(hidden=True)
    async def shutdown(self):
        """
        Command for shutting down bot, only bot author can use it,
        usage:
            type :prefix:manage shutdown
        :return:
        """
        await self.bot.say("You killed me you sick fuck")
        await self.bot.close()
        self.logger.info("Bot has been shutdown by command")

    @manage.command()
    async def change_description(self, *, desc):
        """
        Changes bot in game status to :param desc: and send message to confirm
        :param desc:
        :return:
        """
        await self.bot.change_presence(game=discord.Game(name=desc))
        await self.bot.say("Pomyślnie zmieniono status")
        self.logger.info("Changed description")

    @manage.command()
    async def load(self, name: str):
        await self.bot.say(self._load(name)[1])

    @manage.command()
    async def reload(self, name: str):
        moduleName = self._unload(name)
        result = self._load(moduleName)
        if result[0]:
            await self.bot.say("Extension reloaded correctly")
        else:
            await self.bot.say(result[1])

    @manage.command()
    async def unload(self, name: str):
        print(self.bot.get_cog(name))
        if self.bot.get_cog(name):
            self._unload(name)
        else:
            await self.bot.say("There is no such extension")

    @manage.command()
    async def extensions(self):
        extensions = self.bot.cogs
        await self.bot.say('\n'.join(extensions))

    @manage.command(name="log-lv")
    async def change_logging_level(self, level: str):
        level = level.upper()
        if level in ('DEBUG', 'INFO', 'WARNING', 'ERROR'):
            self.logger.setLevel(level)
            msg = 'Logging level has been changed to {}'.format(level)
            self.logger.info(msg)
            await self.bot.say(msg)
        else:
            msg = 'There is no such logging level ({})'.format(level)
            self.logger.warning(msg)
            await self.bot.say(msg)

    @manage.command(name='log')
    async def display_log(self, debug=False):
        with open('discord.log') as log:
            log_content = log.read().splitlines()
            log_content_formated = []
            if not debug:
                for line in log_content[:]:
                    if line.startswith("DEBUG"):
                        log_content.remove(line)
            for line in log_content[:]:
                if line.startswith("DEBUG"):
                    log_content_formated.append('#'+line)
                elif line.startswith("ERROR"):
                    log_content_formated.append('[ ]['+line+']')
                elif line.startswith("INFO"):
                    log_content_formated.append('['+line+'][]')
                elif line.startswith("WARNING"):
                    log_content_formated.append('< '+line+'>')
            i = 30
            msg = ''
            while msg.__len__() == 0 or msg.__len__() > 2000:
                msg = '```md\n'+'\n'.join(log_content_formated[-i:])+'```'
                i -= 1
            await self.bot.say(msg)

    def _unload(self, name):
        cog = self.bot.get_cog(name)
        self.bot.remove_cog(name)
        self.bot.unload_extension(cog.__module__)
        self.logger.info('Unloaded extension {}'.format(cog.__module__))
        return cog.__module__

    def _load(self, name):
        try:
            self.bot.load_extension(name)
            result = True, 'Loaded extension {}'.format(name)
        except Exception as e:
            exc = '{}: {}'.format(type(e).__name__, e)
            result = False, 'Failed to load extension {}\t{}'.format(name, exc)
        finally:
            if result[0]:
                self.logger.info(result[1])
            else:
                self.logger.error(result[1])
            return result


def setup(bot):
    bot.add_cog(Manage(bot))
