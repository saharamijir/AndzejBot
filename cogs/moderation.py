import discord
from discord.ext import commands
import logging
from utils.Checks import *
import Models.baseModel as baseModel
from Models.userModel import User
from Models.rolePermissionModel import RolePermission
from Models.settingsModel import Settings
from Models.userRoleModel import UserRole
from Models.userPermissionModel import UserPermision
from Models.permissionsCategoryModel import PermissionsCategory
from Models.permissionModel import Permission
from Models.roleModel import Role
from Models.requestModel import Request
from Models.playlistModel import Playlist


class Moderation():

    def __init__(self, bot):
        self.bot = bot
        self.logger = logging.getLogger('discord')

    @commands.command(pass_context=True)
    @is_owner()
    async def createDb(self, ctx):
        db = baseModel.database
        db.create_tables([User, Permission, PermissionsCategory, Playlist, Request, Role, RolePermission, Settings,
                          UserPermision, UserRole])

    @commands.command(pass_context=True)
    @is_owner()
    async def seed(self, ctx):
        if not PermissionsCategory.select().exists():
            categoriesNames = ["General Permissions", "Text Permissions", "Voice Permissions"]
            for name in categoriesNames:
                permCategory = PermissionsCategory(name=name)
                permCategory.save()
        if not Permission.select().exists():
            general = ["Administrator", "View Audit Log", "Manage Server", "Manage Roles", "Manage Channels",
                       "Kick Members", "Ban Members", "Create Instant Invite", "Change Nickname", "Manage Nicknames",
                       "Manage Emojis", "Manage Webhooks", "Read Text Channels & See Voice Channels"]
            text = ["Send Messages", "Send TTS Messages", "Manage Messages", "Embed Links", "Attach Files",
                    "Read Message History", "Mention Everyone", "Use External Emojis", "Add Reaction"]
            voice = ["Connect", "Speak", "Mute Members", "Deafen Members", "Move Members", "Use Voice Activity"]
            for permission in general:
                perm = Permission(name=permission, category=PermissionsCategory
                                  .get(PermissionsCategory.name == "General Permissions"))
                perm.save()
            for permission in text:
                perm = Permission(name=permission, category=PermissionsCategory
                                  .get(PermissionsCategory.name == "Text Permissions"))
                perm.save()
            for permission in voice:
                perm = Permission(name=permission, category=PermissionsCategory
                                  .get(PermissionsCategory.name == "Voice Permissions"))
                perm.save()
        if not Role.select().exists():
            for role in ctx.message.server.roles:
                r = Role(name=role.name)
                r.save()

        if not RolePermission.select().exists():
            for role in ctx.message.server.roles:
                if role.permissions.create_instant_invite:
                    rp = RolePermission(role=Role.get(Role.name == role.name),
                                        permission=Permission.get(Permission.name == "Create Instant Invite"))
                    rp.save()
                if role.permissions.administrator:
                    rp = RolePermission(role=Role.get(Role.name == role.name),
                                        permission=Permission.get(Permission.name == "Administrator"))
                    rp.save()
                if role.permissions.view_audit_logs:
                    rp = RolePermission(role=Role.get(Role.name == role.name),
                                        permission=Permission.get(Permission.name == "View Audit Log"))
                    rp.save()
                if role.permissions.manage_server:
                    rp = RolePermission(role=Role.get(Role.name == role.name),
                                        permission=Permission.get(Permission.name == "Manage Server"))
                    rp.save()
                if role.permissions.manage_roles:
                    rp = RolePermission(role=Role.get(Role.name == role.name),
                                        permission=Permission.get(Permission.name == "Manage Roles"))
                    rp.save()
                if role.permissions.manage_channels:
                    rp = RolePermission(role=Role.get(Role.name == role.name),
                                        permission=Permission.get(Permission.name == "Manage Channels"))
                    rp.save()
                if role.permissions.kick_members:
                    rp = RolePermission(role=Role.get(Role.name == role.name),
                                        permission=Permission.get(Permission.name == "Kick Members"))
                    rp.save()

        tempRoles = []
        temp = []
        for r in Role.select():
            tempRoles.append(r.name)

        for p in Permission.select():
            temp.append(p.name + ' ' + p.category.name)
        await self.bot.say("\n".join(temp))
        await self.bot.say("\n".join(tempRoles))


def setup(bot):
    bot.add_cog(Moderation(bot))
