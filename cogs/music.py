import discord
from discord.ext import commands
from utils.YtApi import YtApi
from utils.MusicPlayer import MusicPlayer


class Music():
    def __init__(self, bot):
        self.bot = bot
        self.youtubeApi = YtApi("AIzaSyCC_1xSjM_QitDQvz1oT25SG8iypHbQ0dc")
        self.musicplayer = MusicPlayer([],
                                       loop=self.bot.loop)

    @commands.group(pass_context=True)
    async def music(self, ctx):
        """
        Commands group for playing music
        :param ctx:
        :return:
        """

        if ctx.invoked_subcommand is None:
            await self.bot.say("I czego ty kurwa ode mnie chcesz?")

    @music.command(help="Odtwórz muzykę", pass_context=True)
    async def play(self, ctx, musicUrl='', *args):
        """
        Command for playing music
        :param ctx: Discord context
        :param musicUrl: Url for song to play
        :param args: Optional voice channel to which bot connects, required when user calling this method isn't
            connected to voice channel
        :return:
        """
        print(self.youtubeApi.is_yt_playlist(musicUrl))
        if ctx.message.author.voice.voice_channel is not None:
            if discord.opus.is_loaded():
                if not self.bot.is_voice_connected(ctx.message.server):
                    voice = await self.bot.join_voice_channel(ctx.message.author.voice.voice_channel)
                    self.musicplayer.AddVoiceConnection(voice)

                if musicUrl:
                    if self.youtubeApi.is_yt_playlist(musicUrl):
                        for url in self.youtubeApi.fetch_all_videos_urls_from_url(musicUrl):
                            self.musicplayer.AddToPlaylist(url)
                    elif 'v=' in musicUrl:
                        self.musicplayer.AddToPlaylist(musicUrl)
                await self.musicplayer.Play()

    @music.command(help="Pomiń obecną piosenkę")
    async def skip(self):
        self.musicplayer.Skip()
        await self.bot.say("Pominięto piosenkę")

    @music.command(help="Zatrzymaj odtwarzanie", pass_context=True)
    async def stop(self):
        await self.musicplayer.Stop()
        await self.bot.say("Zatrzymano odtwarzanie")

    @music.command(help="Lista odtwarzania")
    async def queue(self):
        qlist = '\n'.join(map(str, self.musicplayer.Queue()[:25]))
        qlist = 'Playlist:\n' + qlist
        await self.bot.say(qlist)

    @music.command(help="Obecnie odtwarzana piosenka")
    async def playing(self):
        await self.bot.say(self.musicplayer.currnetSong())


def setup(bot):
    bot.add_cog(Music(bot))


