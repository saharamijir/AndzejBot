""" Pull All Youtube Videos from a Playlist """

from apiclient.discovery import build
from apiclient.errors import HttpError
from oauth2client.tools import argparser


class YtApi:

    def __init__(self, apiKey):
        self.DEVELOPER_KEY = apiKey
        self.YOUTUBE_API_SERVICE_NAME = "youtube"
        self.YOUTUBE_API_VERSION = "v3"

    def fetch_all_videos(self, playlistId):
        """
        Fetches a playlist of videos from youtube
        We splice the results together in no particular order

        Parameters:
            parm1 - (string) playlistId
        Returns:
            playListItem Dict
        """
        youtube = build(self.YOUTUBE_API_SERVICE_NAME,
                        self.YOUTUBE_API_VERSION,
                        developerKey=self.DEVELOPER_KEY)
        result = youtube.playlistItems().list(
            part="snippet",
            playlistId=playlistId,
            maxResults="50"
        ).execute()

        nextPageToken = result.get('nextPageToken')
        while 'nextPageToken' in result:
            nextPage = youtube.playlistItems().list(
                part="snippet",
                playlistId=playlistId,
                maxResults="50",
                pageToken=nextPageToken
            ).execute()
            result['items'] = result['items'] + nextPage['items']

            if 'nextPageToken' not in nextPage:
                result.pop('nextPageToken', None)
            else:
                nextPageToken = nextPage['nextPageToken']

        return result

    def fetch_all_videos_urls(self, playlistId):
        """
        Fetches a playlist of videos from youtube and returns links
        :param playlistId: (string) playlistId
        :return:
        """
        videos = self.fetch_all_videos(playlistId)
        for video in videos['items']:
            yield 'https://www.youtube.com/watch?v='+video['snippet']['resourceId']['videoId']

    def fetch_all_videos_urls_from_url(self, playlistUrl):
        """
        Fetches a playlist of videos from youtube and returns links
        :param playlistUrl:
        :return:
        """
        if not self.is_yt_playlist(playlistUrl):
            raise ValueError('Url is not a playlist')
        for part in playlistUrl.split('?')[1].split('&'):
            if 'list=' in part:
                for result in self.fetch_all_videos_urls(part.split('=')[1]):
                    yield result

    def is_yt_playlist(self, url):
        if 'list=' not in url or 'youtube' not in url:
            return False
        return True
