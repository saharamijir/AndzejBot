import discord
from discord.ext import commands

def is_owner():
    """
    Check if message author is bot author, change variable author_id to your discord id
    :param ctx:
    :return:
    """
    def predicate(ctx):
        author_id = '239030086840483840'
        return ctx.message.author.id == author_id
    return commands.check(predicate)
