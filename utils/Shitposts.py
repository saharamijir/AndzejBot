import basc_py4chan
import random
import urllib.request


class FourChan:
    def RandomShitpost(self):
        """
        Returns local name of random image from 4Chan random
        """
        FourChan = basc_py4chan.Board('b')
        Threads = FourChan.get_all_thread_ids()
        Thread = FourChan.get_thread(random.choice(Threads))
        URLs = []
        for f in Thread.file_objects():
            URLs.append(f.file_url)
        link = random.choice(URLs)
        filename = link.split('/')[-1]
        urllib.request.urlretrieve(link, filename)
        return filename
