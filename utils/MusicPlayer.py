from enum import Enum
import asyncio
import logging

class MusicPlayer:
    def __init__(self, playlist=None, voiceConnection=None, loop=None):
        self.playlist = playlist
        self.voiceConnection = voiceConnection
        self.status = PlayerStatus.STOPPED
        self.currentSong = 0
        self.player = None
        self.loop = loop
        self.playLoop = LoopStatus.ALLSONGS
        self.logger = logging.getLogger("discord")

    async def Play(self):
        if self.status == PlayerStatus.STOPPED:
            self.status = PlayerStatus.PLAYING
            self.player = await self.voiceConnection.create_ytdl_player(self.playlist[self.currentSong], after=lambda: CheckPlayingStatus())
            self.player.start()

    def AddToPlaylist(self, ytUrl):
        self.playlist.append(ytUrl)

    def AddVoiceConnection(self, voiceConnection):
        self.voiceConnection = voiceConnection

    def CheckPlayingStatus(self):
        self.currnetSong += 1
        self.CheckPlaylistStatu()
        if self.status == PlayerStatus.PLAYING:
            coro = self.voiceConnection.create_ytdl_player(self.playlist[self.currnetSong],
                                                           after=self.CheckPlayingStatus)
            fut = asyncio.run_coroutine_threadsafe(coro, self.loop)
            try:
                self.player = fut.result()
            except:
                self.logger.error("something went wrong trying to play song")

            self.player.start()

    def CheckPlaylistStatu(self):
        if self.playLoop == LoopStatus.ALLSONGS and self.currnetSong == len(self.playlist):
            self.currnetSong = 0

    async def Stop(self):
        self.status = PlayerStatus.STOPPED
        self.player.stop()

        await self.voiceConnection.disconnect()

    def Skip(self):
        self.player.stop()

    def Queue(self):
        return self.playlist

    def CurrentSongUrl(self):
        return self.playlist[self.currnetSong]

class PlayerStatus(Enum):
    STOPPED = 1
    PLAYING = 2
    PAUSED = 3

class LoopStatus(Enum):
    ONCE = 1
    ALLSONGS = 2
    ONESONG = 3
