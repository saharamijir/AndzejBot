import requests
import json
from statistics import mean
from collections import namedtuple


class BladeAndSoulApi:
    def __init__(self):
        self._BASEURL = 'https://api.silveress.ie/bns/v3/'
        self._CHARACTERENDPOINT = 'character/full/'
        self._CHARACTERRANKINGENDPOINT = 'character/ranking/'
        self._MARKETENDPOINT = 'market/'
        self._ITEMSENDPOINT = 'items/'
        self._EQUIPMENTENDPOINT = 'equipment/'

    def GetCharacter(self, region, characterName):
        fullUrl = self._BASEURL + 'character/full/' + region + '/' + characterName
        response = requests.get(fullUrl)
        return json.loads(response.text)

    def GetItems(self):
        requestUrl = self._BASEURL + self._ITEMSENDPOINT
        response = requests.get(requestUrl)
        return json.loads(response.text)

    def GetItemIdByName(self, name):
        for item in self.GetItems():
            if item["name"] == name:
                return item["id"]

    def GetItemInfoByName(self, name):
        for item in self.GetItems():
            if item["name"] == name:
                return item

    def GetItemInfoById(self, id):
        for item in self.GetItems():
            if item["id"] == id:
                return item

    def GetItemMarketInfoByName(self, name, region):
        itemId = self.GetItemIdByName(name)
        fullUrl = self._BASEURL + self._MARKETENDPOINT + region + '/current/' + str(itemId)
        response = requests.get(fullUrl)
        return json.loads(response.text)

    def GetItemPrices(self, name, region):
        itemInfo = self.GetItemMarketInfoByName(name, region)
        price = []
        for auction in itemInfo[0]["listings"]:
            price.append(auction["each"])
        PriceInfo = namedtuple('PriceInfo', ['mean', 'max', 'min'])(mean(price), max(price), min(price))
        return PriceInfo
